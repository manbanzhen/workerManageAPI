"""workermanage URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Worker import views, stuView

# 绑定跳转的路径与方法
urlpatterns = [
    path('admin/', admin.site.urls),
    # worker manage system
    # path(r"api/worker/", views.workers.as_view()),
    # path(r"api/del_worker/", views.del_worker),
    # path(r"api/search/", views.search),
    # path(r"api/getClass/", views.getClass),
    # path(r"api/getOneWorker/", views.getOneWorker),

    # students manage system
    path(r"api/get_subject/", stuView.getSubject),
    path(r"api/student/", stuView.Student),
    path(r"api/del_student/", stuView.deleteStudent),
    path(r"api/search_student/", stuView.searchStudent),
    path(r"api/result/", stuView.Result),
    path(r"api/update_result/", stuView.updateResult),
    path(r"api/have_subject/", stuView.haveSubject),
    path(r"api/del_subject/", stuView.delResult),
    path(r"api/get_one_student/", stuView.getOneStudent),
]
