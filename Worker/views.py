from django.http import JsonResponse
# 导入实体类所在的包
# 导入实体类的模块
from Worker import models
# 导入模糊查询
from django.db.models import Q
# 导入实体类
from django.views import View
# 解析put请求数据
from django.http.multipartparser import MultiPartParser


# 全局定义返回格式
def resData(code, data):
    res = {'code': code, 'data': data}
    return res


class workers(View):
    # 获取所有员工
    def get(self, request, *args, **kwargs):
        workerObj = models.Worker.objects.all()
        # print(workerObj)
        data = []
        for i in workerObj:
            item = {}
            item['id'] = i.id
            item['name'] = i.name
            item['age'] = i.age
            item['wages'] = i.wage
            item['dept'] = i.dept.name
            data.append(item)
        return JsonResponse(resData(0, data))

    # 增加员工
    def post(self, request, *args, **kwargs):
        req = request.POST
        name = req['name']
        age = req['age']
        wage = req['wage']
        dept = req['dept']
        print(name, age, wage, dept)
        deptObj = models.classType.objects.get(id=int(dept))
        models.Worker(name=name, age=age, wage=wage, dept=deptObj).save()
        return JsonResponse(resData(0, ''))

    # 修改员工信息
    def put(self, request, *args, **kwargs):
        req = MultiPartParser(request.META, request, request.upload_handlers).parse()[0]
        id = req['id']
        name = req['name']
        age = req['age']
        wage = req['wage']
        dept = req['dept']
        wObj = models.Worker.objects.get(id=int(id))
        wObj.name = name
        wObj.age = age
        wObj.wage = wage
        try:
            deptObj = models.classType.objects.get(id=int(dept))
            wObj.dept = deptObj
        except:
            pass
        wObj.save()
        return JsonResponse(resData(0, ''))


# 删除指定的员工
def del_worker(request):
    if request.method == 'POST':
        id = request.POST['id']
        models.Worker.objects.get(pk=id).delete()
        return JsonResponse(resData(0, ''))


# 模糊查询
def search(request):
    query = request.GET.get('query')
    searchObj = models.Worker.objects.filter(Q(name__icontains=query))
    data = []
    for i in searchObj:
        item = {}
        item['id'] = i.id
        item['name'] = i.name
        item['age'] = i.age
        item['wages'] = i.wage
        item['dept'] = i.dept.name
        data.append(item)
    return JsonResponse(resData(0, data))


def getOneWorker(request):
    # try:
    id = request.GET.get('id')
    wObj = models.Worker.objects.get(pk=id)
    data = []
    item = {}
    item['id'] = wObj.id
    item['name'] = wObj.name
    item['age'] = wObj.age
    item['wages'] = wObj.wage
    item['dept'] = wObj.dept.name
    data.append(item)
    return JsonResponse(resData(0, data))
    # except Exception as e:
    #     return JsonResponse(resData(500, e))


# 获取所有部门数据
def getClass(request):
    try:
        cObj = models.classType.objects.all()
        data = []
        for i in cObj:
            item = {}
            item['id'] = i.id
            item['name'] = i.name
            data.append(item)
        return JsonResponse(resData(0, data))
    except Exception as e:
        return JsonResponse(resData(500, e))












# Create your views here.
# 主页显示所有学生的信息
# def showAll(request):
#     workers = Worker.objects.all()
#     count = workers.__len__()
#     # 返回网页地址并携带学生数据
#     return render(request, "index.html", context={"workers": workers, "count": count})
#
#
# # 根据首页输入框模糊查询
# def findWorker(request):
#     # 获取搜索框的值
#     str = request.POST.get("str")
#     # 模糊查询(根据姓名或者性别查询)
#     workers = models.Worker.objects.filter(
#         Q(name__icontains=str) | Q(id__icontains=str))
#     # 获取数据的总条数
#     count = workers.__len__()
#     return render(request, "index.html", context={"workers": workers, "count": count})
#
#
# # 添加学生
# def addWorker(request):
#     if request.method == "GET":
#         # 通过提交的方式判断
#         # 如果是首页点击添加，则跳转到add.html页面
#         return render(request, "add.html")
#     else:
#         # 如果是添加页面提交数据。则添加到数据库
#         id=request.POST.get("id")
#         name = request.POST.get("name")
#         sex = request.POST.get("sex")
#         wage = request.POST.get("age")
#         part=request.POST.get("part")
#         # 使用Django框架提供的添加对象的方法
#         Worker.objects.create(id=id,name=name, sex=sex, wage=wage,part=part)
#         # 重定向到index.html
#         return redirect("index.html")
#
#
# # 修改学生
# def updateWorker(request):
#     # 根据表单提交的方式判断是查询单个还是修改之后提交数据库
#     if request.method == "GET":
#         # 获取要修改的对象的sid查询单个的学生
#         id = request.GET['update_id']
#         #  根据学生的sid查询单个学生进行修改
#         worker = Worker.objects.get(id=id)
#         # 跳转到修改页面，并携带修改对象的信息
#         return render(request, "update.html", context={"worker": worker})
#     # 表单提交，进行修改学生
#     else:
#         # 获取需要修改的学生对象的信息
#         update_id=request.POST.get("sid")
#         update_worker = Worker.objects.get(sid=update_id)
#         update_name=request.POST.get("name")
#         update_sex=request.POST.get("sex")
#         update_wage=request.POST.get("wage")
#         update_part=request.POST.get("part")
#         # 修改对象的信息
#         update_worker.id=update_id
#         update_worker.name=update_name
#         update_worker.sex=update_sex
#         update_worker.wage=update_wage
#         update_worker.part=update_part
#         # 保存对象到数据库
#         update_worker.save()
#         # 重定向到首页，显示学生信息
#         return redirect("index.html")
#
# # 删除学生
# def deleteWorker(request):
#     # 获取需要删除的学生对象的sid
#     delete_id=request.GET['delete_id']
#     # 先查找单个对象，然后进行删除
#     Worker.objects.get(sid=delete_id).delete()
#     # 删除之后，重定向到首页
#     return redirect("index.html")


