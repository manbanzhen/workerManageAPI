from django.db import models


class Students(models.Model):
    id = models.AutoField(primary_key=True, verbose_name="id")
    stuNumber = models.IntegerField(unique=True, default='0', verbose_name="学号")
    name = models.CharField(max_length=33, verbose_name="学生姓名")
    classes = models.CharField(max_length=123, verbose_name="班级")

    def __str__(self):
        return self.name


class Subject(models.Model):
    id = models.AutoField(primary_key=True, verbose_name="科目号")
    name = models.CharField(max_length=99, verbose_name="科目名称")

    def __str__(self):
        return self.name


class Results(models.Model):
    id = models.AutoField(primary_key=True)
    usual = models.CharField(max_length=22, verbose_name="平时成绩")
    final = models.CharField(max_length=22, verbose_name="期末成绩")
    total = models.CharField(max_length=22, verbose_name="总成绩")
    stu_num = models.ForeignKey(Students, on_delete=models.CASCADE, verbose_name="学号")
    sub_num = models.ForeignKey(Subject, on_delete=models.CASCADE, verbose_name="科目号")
    
    




# class classType(models.Model):
#     id = models.AutoField(primary_key=True)
#     name = models.CharField(max_length=123, verbose_name='部门名称')


# class Worker(models.Model):
#     id = models.AutoField(primary_key=True)
#     name = models.CharField(max_length=255, blank=True, null=True, verbose_name='员工名称')
#     # sex = models.CharField(max_length=255, blank=True, null=True)
#     age = models.IntegerField(blank=True, null=True, verbose_name='员工年龄')
#     wage = models.IntegerField(blank=True, null=True, verbose_name='员工薪资')
#     # part = models.CharField(max_length=255, blank=True, null=True, verbose_name='所属部门')
#     dept = models.ForeignKey(classType, on_delete=models.CASCADE, verbose_name="所属部门")
