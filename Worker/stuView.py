from django.http import JsonResponse
# 导入实体类所在的包
# 导入实体类的模块
from Worker import models
# 导入模糊查询
from django.db.models import Q


# 全局定义返回格式
def resData(code, data):
    res = {'code': code, 'data': data}
    return res


def getSubject(request):
    if(request.method.lower() == 'get'):
        try:
            cObj = models.Subject.objects.all()
            print(cObj)
            data = []
            for i in cObj:
                item = {}
                item['sub_id'] = i.id
                item['sub_name'] = i.name
                data.append(item)
            return JsonResponse(resData(0, data))
        except Exception as e:
            return JsonResponse(status=500, data={})
    else:
        return JsonResponse(status=502, data={},)


def Student(request):
    if (request.method.lower() == 'get'):
        stuObj = models.Students.objects.all()
        data = []
        for i in stuObj:
            item = {}
            item['id'] = i.id
            item['name'] = i.name
            item['classes'] = i.classes
            item['stuNumber'] = i.stuNumber
            data.append(item)
        return JsonResponse(resData(0, data))

    elif (request.method.lower() == 'post'):
        res = request.POST
        try:
            stuObj = models.Students(name=res['name'], classes=res['classes'], stuNumber=res['stuNumber'])
            stuObj.save()
            return JsonResponse(resData(0, {"stu_id": stuObj.id, "stu_name": stuObj.name, "classes": stuObj.classes, "stuNumber":stuObj.stuNumber}))
        except Exception as e:
            print(e)
            return JsonResponse(status=500, data={"error": '新增学生失败'})
    else:
        return JsonResponse(status=502, data={},)


def deleteStudent(request):
    if (request.method.lower() == 'get'):
        stu_id = request.GET.get('stu_id')
        stuObj = models.Students.objects.get(pk=stu_id)
        stuObj.delete()
        return JsonResponse(resData(0, {}))


def getOneStudent(request):
    if (request.method.lower() == 'get'):
        stu_id = request.GET.get('stu_id')
        results = models.Results.objects.filter(stu_num=stu_id)
        data = {}
        stuObj = models.Students.objects.get(pk=stu_id)
        data['student'] = {"stu_id": stuObj.id, "stu_name": stuObj.name, "classes": stuObj.classes}
        resList = []
        for i in results:
            item = {}
            item['sub_id'] = i.sub_num.id
            item['sub_name'] = i.sub_num.name
            item['usual'] = i.usual
            item['final'] = i.final
            item['total'] = i.total
            resList.append(item)
        data['results'] = resList
        return JsonResponse(resData(0, data))


def Result(request):
    if (request.method.lower() == 'get'):
        try:
            stu_id = request.GET.get('stu_id')
            sub_id = request.GET.get('sub_id')
            resultObj = models.Results.objects.get(stu_num=stu_id, sub_num=sub_id)

            data = []
            item = {}
            item['stu_id'] = resultObj.stu_num.id
            item['stu_name'] = resultObj.stu_num.name
            item['sub_id'] = resultObj.sub_num.id
            item['sub_name'] = resultObj.sub_num.name
            item['usual'] = resultObj.usual
            item['final'] = resultObj.final
            item['total'] = resultObj.total
            data.append(item)
            return JsonResponse(resData(0, data))
        except Exception as e:
            print('errpr: ', e)
            return JsonResponse(status=500, data={"error": '获取失败'})
    elif (request.method.lower() == 'post'):
        res = request.POST
        try:
            stu_id = res['stu_id']
            sub_id = res['sub_id']
            usual = res['usual']
            final = res['final']
            total = res['total']
            try:
                models.Results.objects.get(stu_num__id=stu_id, sub_num__id=sub_id)
                return JsonResponse(resData(500, {"error": "已经存在此科目成绩"}))
            except Exception as e:
                # 获取学生对象
                stuObj = models.Students.objects.get(pk=stu_id)
                # 获取学科对象
                subObj = models.Subject.objects.get(pk=sub_id)
                # 存储此科目成绩
                resultObj = models.Results(usual=usual, final=final, total=total, stu_num=stuObj, sub_num=subObj)
                resultObj.save()
                return JsonResponse(resData(0, {}))
        except Exception as e:
            print('error: ', e)
            return JsonResponse(status=500, data={"error": '新增成绩失败'})
    else:
        return JsonResponse(status=502, data={}, )


def updateResult(request):
    if (request.method.lower() == 'post'):
        res = request.POST
        try:
            stu_id = res['stu_id']
            sub_id = res['sub_id']
            usual = res['usual']
            final = res['final']
            total = res['total']
            resultObj = models.Results.objects.get(stu_num=stu_id, sub_num=sub_id)
            resultObj.usual = usual
            resultObj.final = final
            resultObj.total = total
            resultObj.save()
            return JsonResponse(resData(0, {}))
        except Exception as e:
            print(e)
            return JsonResponse(status=500, data={"error": '修改成绩失败'})
    else:
        return JsonResponse(status=502, data={}, )


def delResult(request):
    if (request.method.lower() == 'get'):
        try:
            stu_id = request.GET.get('stu_id')
            sub_id = request.GET.get('sub_id')
            print(stu_id, sub_id)
            resultObj = models.Results.objects.get(stu_num=stu_id, sub_num=sub_id)
            resultObj.delete()
            return JsonResponse(resData(0, {}))
        except Exception as e:
            print(e)
            return JsonResponse(status=500, data={"error": '删除科目失败'})

    else:
        return JsonResponse(status=502, data={}, )


def haveSubject(request):
    # 获取当前学生有几门科目
    if (request.method.lower() == 'get'):
        stu_id = request.GET.get('stu_id')
        subObj = models.Results.objects.filter(stu_num=stu_id)
        data = []
        for i in subObj:
            item = {}
            item['sub_id'] = i.sub_num.id
            item['sub_name'] = i.sub_num.name
            data.append(item)
        return JsonResponse(resData(0, data))
    else:
        return JsonResponse(status=502, data={}, )


def searchStudent(request):
    if (request.method.lower() == 'get'):
        query = request.GET.get('query')
        stuObj = models.Students.objects.filter(Q(name__icontains=query))
        data = []
        for i in stuObj:
            item = {}
            item['id'] = i.id
            item['stuNumber'] = i.stuNumber
            item['name'] = i.name
            item['classes'] = i.classes
            data.append(item)
        return JsonResponse(resData(0, data))